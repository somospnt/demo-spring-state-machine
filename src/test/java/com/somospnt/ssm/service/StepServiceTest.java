package com.somospnt.ssm.service;

import com.somospnt.ssm.DemoSpringStateMachineBaseTests;
import com.somospnt.ssm.domain.Estado;
import com.somospnt.ssm.domain.Evento;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.data.jpa.JpaStateMachineRepository;

public class StepServiceTest extends DemoSpringStateMachineBaseTests{

    private static final String MENSAJE_KEY = "mensaje";
    @Autowired
    private JpaStateMachineRepository jpaStateMachineRepository;
    @Autowired
    private StepService stepService;

    @Test
    public void obtener_sinStateMachine_persisteStateMachineYDevuelveStateMachineConEstadoHaciendoFila() {
        String nombreStateMachine = "stateMachine1";

        StateMachine<Estado, Evento> stateMachine = stepService.obtener(nombreStateMachine);

        Assertions.assertThat(stateMachine.getState().getId()).isEqualTo(Estado.HACIENDO_FILA);
        Assertions.assertThat(jpaStateMachineRepository.existsById(nombreStateMachine)).isTrue();
    }
    
    @Test
    public void pedir_conStateMachine_obtieneStateMachineYDevuelveStateMachineConEstado() {
        String nombreStateMachine = "stateMachine2";

        StateMachine<Estado, Evento> stateMachine = stepService.avanzar(nombreStateMachine, Evento.PEDIR);

        Assertions.assertThat(stateMachine.getState().getId()).isEqualTo(Estado.PEDIDO_REALIZADO);
        Assertions.assertThat(stateMachine.getExtendedState().getVariables().get(MENSAJE_KEY)).isEqualTo("Vengo a comprar!");
    }

}
