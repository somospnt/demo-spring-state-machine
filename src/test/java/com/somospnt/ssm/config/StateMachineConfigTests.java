package com.somospnt.ssm.config;

import com.somospnt.ssm.DemoSpringStateMachineBaseTests;
import com.somospnt.ssm.domain.Estado;
import com.somospnt.ssm.domain.Evento;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.test.StateMachineTestPlan;
import org.springframework.statemachine.test.StateMachineTestPlanBuilder;

public class StateMachineConfigTests extends DemoSpringStateMachineBaseTests {

    @Autowired
    private StateMachineFactory<Estado, Evento> stateMachineFactory;
    private static final String MENSAJE_KEY = "mensaje";

    @Test
    public void pedir_desdeHaciendoFila_cambiaAPedidoRealizado() throws Exception {
        StateMachine machine = stateMachineFactory.getStateMachine("mcdonalds");

        StateMachineTestPlan<Estado, Evento> plan = StateMachineTestPlanBuilder.<Estado, Evento>builder()
                .stateMachine(machine)
                .step()
                .expectStates(Estado.HACIENDO_FILA)
                .and()
                .step()
                .sendEvent(Evento.PEDIR)
                .expectStates(Estado.PEDIDO_REALIZADO)
                .expectVariable(MENSAJE_KEY, "Vengo a comprar!")
                .and()
                .step()
                .sendEvent(Evento.RETIRAR_PEDIDO)
                .expectStates(Estado.COMIENDO)
                .expectVariable(MENSAJE_KEY, "Gracias!")
                .and()
                .build();
        plan.test();
    }

}
