CREATE TABLE state_machine (
    machine_id varchar(255) not null,
    state varchar(255),
    state_machine_context longblob,
    primary key (machine_id)
);
