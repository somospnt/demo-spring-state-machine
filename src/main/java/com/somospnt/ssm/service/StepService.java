package com.somospnt.ssm.service;

import com.somospnt.ssm.domain.Estado;
import com.somospnt.ssm.domain.Evento;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

@Service
public class StepService {

    public StateMachine<Estado, Evento> obtener(String nombreStateMachine) {
        //obtener stateMachine por nombre. ¿que pasa si no existe?
        return null;
    }

    public StateMachine<Estado, Evento> avanzar(String nombreStateMachine, Evento evento) {
        //obtener stateMachine por nombre.
        //hacerla avanzar. ¿que pasa si no puede avanzar?
        return null;
    }
}
