package com.somospnt.ssm.controller;

import com.somospnt.ssm.domain.Estado;
import com.somospnt.ssm.domain.Evento;
import com.somospnt.ssm.service.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StepRestController {

    @Autowired
    private StepService stepService;

    @GetMapping("/{nombreStateMachine}")
    public Estado obtener(@PathVariable String nombreStateMachine) {
        return stepService.obtener(nombreStateMachine).getState().getId();
    }

    @PutMapping("/{nombreStateMachine}/pedir")
    public Estado pedir(@PathVariable String nombreStateMachine) {
        return stepService.avanzar(nombreStateMachine, Evento.PEDIR).getState().getId();
        
    }
}

