package com.somospnt.ssm.config;

import com.somospnt.ssm.domain.Estado;
import com.somospnt.ssm.domain.Evento;
import java.util.EnumSet;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends StateMachineConfigurerAdapter<Estado, Evento> {

    @Autowired
    private StateMachineRuntimePersister<Estado, Evento, String> stateMachineRuntimePersister;

    @Override
    public void configure(StateMachineStateConfigurer<Estado, Evento> states)
            throws Exception {
        states
                .withStates()
                .initial(Estado.HACIENDO_FILA)
                .states(EnumSet.allOf(Estado.class))
                .end(Estado.ME_FUI);
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<Estado, Evento> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(Estado.HACIENDO_FILA).target(Estado.PEDIDO_REALIZADO).event(Evento.PEDIR).action(escribirMensaje("Vengo a comprar!"))
                .and()
                .withExternal()
                .source(Estado.PEDIDO_REALIZADO).target(Estado.COMIENDO).event(Evento.RETIRAR_PEDIDO).action(escribirMensaje("Gracias!"));
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<Estado, Evento> stateMachineConfiguration)
            throws Exception {
        stateMachineConfiguration
                .withPersistence()
                .runtimePersister(stateMachineRuntimePersister);
    }

    private static Action<Estado, Evento> escribirMensaje(String mensaje) {
        return context -> {
            Map<String, Object> formData = (Map) context.getExtendedState().getVariables();
            formData.put("mensaje", mensaje);
            System.out.println("Sr. McDonalds! " + mensaje);
        };
    }

}
