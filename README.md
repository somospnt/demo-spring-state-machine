# demo-spring-state-machine

Demo de proyecto con Spring State Machine.

## ¿Qué es Spring State Machine?

Se basa en el concepto de *maquina de estados*. La idea es que la aplicacion tenga un numero finito de estados y ciertos eventos predefinidos que los hacen cambiar (transiciones).

[Más info sobre Máquinas de Estados.](https://es.wikipedia.org/wiki/M%C3%A1quina_de_estados)

[Documentación de Spring State Machine.](https://projects.spring.io/spring-statemachine/)

## Como correrlo

- Darle **RUN** al proyecto

- Para verificar la persistencia de las state machine accede a:

```html
 localhost:8080/h2-console/
```

- Luego conectarse a la siguiente url: 

```html
jdbc:h2:mem:techie
```